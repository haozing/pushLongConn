# pushLongConn

#### 介绍
最简单的HTTP接口推送消息给长连接

#### 命令行

```
pushLongConn -addr localhost:808
```
-addr 是服务启动的地址


#### Websocket接口
##### ws连接[可选择自带参数uid]
/connect

#### HTTP GET接口
##### 推送全部
/pushAll?text=XXX
##### 推送部分[根据自带参数uid]
/push?text=XXX&ids=1,2
##### 推送部分[根据默认参数fd,需要业务将id同fd绑定]
/push?text=XXX&fds=1605854647166177280,1605854647166177283
#### 独立部署
```
nohup ./pushLongConn_amd64 -addr 127.0.0.1:50001 &
```
注意：请注意设置权限，否则可能会出现无法启动的情况

#### nginx配置代理
##### ws配置
``` 
location ^~ /connect {
    proxy_pass http://127.0.0.1:50001/connect;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
}
``` 
##### http配置
``` 
location ^~ /pushAll {
    proxy_pass http://127.0.0.1:50001/pushAll;
}
``` 
#### 编译
##### 在Windows下编译 Linux
```
 set GOARCH=amd64
 set GOOS=linux
 go build -o pushLongConn_amd64
```
```
 go env -w CGO_ENABLED=0 GOOS=linux GOARCH=amd64
 go build -o pushLongConn_amd64
```
##### 在windows下编译 Mac
```
SET GOOS=darwin
SET GOARCH=amd64
go build -o pushLongConn_mac
```
##### 在Linux下编译 Windows
```
 set GOARCH=amd64
 set GOOS=windows
 go build -o pushLongConn_windows.exe
```
##### 在Linux下编译 Mac
```
 set GOARCH=amd64
 set GOOS=darwin
 go build -o pushLongConn_mac
```

